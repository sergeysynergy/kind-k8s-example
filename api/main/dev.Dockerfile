FROM python:3.8-slim-buster

RUN pip install --upgrade pip
RUN pip install flask flask_cors dapr

WORKDIR /workdir
COPY . .

EXPOSE 7000

CMD /bin/sh -c "python app.py && while true; do echo faking daemon; sleep 600; done"
