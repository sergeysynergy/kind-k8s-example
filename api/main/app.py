import flask

app = flask.Flask(__name__)

@app.route("/")
def hello_api():
    return "The best API ever!\n"

app.run(host='0.0.0.0', port=7000)
