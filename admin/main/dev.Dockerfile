FROM python:3.7.6-alpine

WORKDIR /workdir
# Contemporary needed packages
RUN apk --update add vim postgresql-dev libxslt-dev gcc openjpeg-dev tk-dev curl
# Packages for builing image
RUN apk add libxml2-dev libffi-dev musl-dev libgcc openssl-dev curl jpeg-dev zlib-dev freetype-dev lcms2-dev
RUN apk add tiff-dev tcl-dev g++ build-base

# Installing Python modules
RUN pip install --upgrade pip
COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# Packages for debug
RUN pip install watchdog psutil

# Remove packages for building
RUN apk del libxml2-dev libffi-dev musl-dev libgcc openssl-dev curl jpeg-dev zlib-dev freetype-dev lcms2-dev
RUN apk del tcl-dev g++ build-base

# Copy files for execution
COPY . .

EXPOSE 8000

# Start fake service for debud
# CMD /bin/sh -c "while true; do echo faking daemon; sleep 600; done"

# Start develop web server
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
