FROM nginx

RUN apt-get update && apt-get install -y procps vim inetutils-ping

WORKDIR /workdir
COPY . .
COPY ./nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

# For NGINX conf test:
# CMD /bin/sh -c "while true; do echo faking daemon; sleep 600; done"

