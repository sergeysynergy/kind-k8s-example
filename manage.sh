#!/bin/bash

help() {
  cat << EOF
This is a tool for managing project based on kind - local k8s cluster.

Usage:
  ./manage.sh [command]

Available Commands:
  up dev      - build and start kind cluster with developer settings
  up prod     - build and start kind cluster with production settings
  info        - useful information about cluster
  check       - simple health check
  stop        - stop cluster
  purge       - delete all built infrastructure

EOF
}


# Docker registry port
regPort='5000'
# Docker registry name
regName='registry-kind'


upDockerRegistry() {
  # Check registry exist, create if needed
  running="$(docker inspect -f '{{.State.Running}}' "${regName}" 2>/dev/null || true)"
  if [ "${running}" == "false" ]; then
    docker start ${regName}
  else
    if [ "${running}" != "true" ]; then
      docker run \
        -d --restart=always -p "${regPort}:5000" --name "${regName}" \
        registry:2
      docker network connect "kind" "registry-kind"
    fi
  fi
}


upDockerImages() {
  echo "Building images and pushing them in docker registry"
  echo ""

  echo "Admin image"
  name="admin:1.0"
  cd ./admin/main
  docker build -t localhost:${regPort}/${name} -f ./${1}.Dockerfile .
  docker push localhost:${regPort}/${name}
  # docker run -p 8000:8000 --name admin --volume=/home/zs/work/erp-workspace/kind-k8s-example/admin/main:/workdir localhost:5000/${name}  # 4testing
  # docker run -p 8000:8000 --name admin localhost:5000/${name}  # 4testing
  cd ../../

  echo "API image"
  name="api:1.0"
  cd ./api/main
  docker build -t localhost:${regPort}/${name} -f ./${1}.Dockerfile .
  docker push localhost:${regPort}/${name}
  cd ../../

  echo "Client image"
  name="client:1.0"
  cd ./client/main
  docker build -t localhost:${regPort}/${name} -f ./${1}.Dockerfile .
  docker push localhost:${regPort}/${name}
  cd ../../
}


upCluster() {
  echo ""
  echo "Create kind cluster"
  name=$(kind get clusters)
  if [ "${name}" != 'kind' ]; then
    # kind create cluster --config cluster.yaml
    kind create cluster --config ./env/cluster.yml --image kindest/node:v1.18.0
  else
    echo "Cluster kind already exists"
  fi
  echo ""
}


upConfig() {
  echo "Apply services configurations"
  cd ./env

  kubectl apply -f admin-dep.yml
  kubectl apply -f api-dep.yml
  kubectl apply -f client-dep.yml

  echo ""
  cd ../
}


upIngress() {
  echo "Apply NGINX-based ingress controller"
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
  kubectl apply -f ./env/ingress.yml
  echo ""
}


if [[ $1 = "up" ]]; then

  if [[ $2 = "dev" ]]; then
    env="dev"
  elif [[ $2 = "prod" ]]; then
    env="prod"
  else
    echo "Chose proper environment: dev or prod"
    exit 1
  fi

  upDockerRegistry
  upDockerImages ${env}
  upCluster
  upConfig
  upIngress

  cat << EOF

Kind cluster ready for usage:
Services started:
  - API: http://localhost/api

EOF

  echo "Nodes:"
  kubectl get nodes -o wide
  echo ""

  echo "Pods:"
  kubectl get pods -o wide
  echo ""

  exit 0
fi


if [[ $1 = "upin" ]]; then
  upIngress
  exit 0
fi


if [[ $1 = "info" ]]; then
  echo ""
  echo "Containers in docker registry:"
  curl -X GET localhost:${regPort}/v2/_catalog
  curl -X GET localhost:${regPort}/v2/api/tags/list
  curl -X GET localhost:${regPort}/v2/client/tags/list
  curl -X GET localhost:${regPort}/v2/admin/tags/list
  echo ""

  echo "Kind clusters:"
  kind get clusters
  echo ""

  echo "Nodes:"
  kubectl get nodes -o wide
  echo ""

  echo "Pods:"
  # kubectl get pods -n draft -o wide
  kubectl get pods -o wide
  echo ""

  # echo "Volumes:"
  # kubectl get pvc
  # echo ""

  # echo "Control node info:"
  # kubectl describe node erp-control-plane
  # echo ""

  exit 0
fi

if [[ $1 = "check" ]]; then
  echo "Testing URL's:"
  echo ""

  echo "localhost:"
  curl localhost
  echo ""
  echo ""

  echo "localhost/api:"
  curl localhost/api
  echo ""

  # echo "localhost/admin/:"
  # curl localhost/admin/
  echo ""

  exit 0
fi


if [[ $1 = "purge" ]]; then
  kind delete cluster
  echo 'Deleting Docker registry "registry-kind"...'
  docker container stop ${regName} > /dev/null && docker container rm -v ${regName} > /dev/null
  exit 0
fi


help